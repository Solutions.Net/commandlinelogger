﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using System.Threading;

namespace CommandLineLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            //System.Threading.Thread.Sleep(10000);
            var exe = System.Reflection.Assembly.GetEntryAssembly().Location;
            // For testing
            // exe = @"C:\Portables\Git\bin\CommandLineLogger\git.exe";
            var folder = Path.GetDirectoryName(exe);
            folder = Path.GetDirectoryName(folder);
            var exeToWatch = Path.Combine(folder, Path.GetFileName(exe));

            var realCurrentExe = System.Reflection.Assembly.GetEntryAssembly().Location;
            if (Environment.CommandLine.StartsWith("\""))
                realCurrentExe = "\"" + realCurrentExe + "\"";
            Debug.Assert(Environment.CommandLine.StartsWith(realCurrentExe));
            Execute(/*folder, */exeToWatch, Environment.CommandLine.Substring(realCurrentExe.Length).Trim());
        }

        static int Execute(/*string workingDir, */string processPath, string allArguments)
        {
            // from http://www.c-sharpcorner.com/article/redirecting-standard-inputoutput-using-the-process-class/
            var psi = new ProcessStartInfo();
            //psi.WorkingDirectory = workingDir;
            psi.FileName = processPath;
            psi.Arguments = allArguments;

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true; // only taken in account if UseShellExecute is false
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            Process p = new Process();
            p.StartInfo = psi;
            p.EnableRaisingEvents = true; // https://msdn.microsoft.com/en-us/library/system.diagnostics.process.enableraisingevents(v=vs.110).aspx
            // TODO : https://stackoverflow.com/questions/33716580/process-output-redirection-on-a-single-console-line

            // Code inspired from https://stackoverflow.com/a/30517342
            // Depending on your application you may either prioritize the IO or the exact opposite
            const ThreadPriority ioPriority = ThreadPriority.Highest;
            var outputThread = new Thread(outputReader) { Name = "ChildIO Output", Priority = ioPriority };
            var errorThread = new Thread(errorReader) { Name = "ChildIO Error", Priority = ioPriority };
            var inputThread = new Thread(inputReader) { Name = "ChildIO Input", Priority = ioPriority };

            // Set as background threads (will automatically stop when application ends)
            outputThread.IsBackground = errorThread.IsBackground = inputThread.IsBackground = true;

            _start = DateTime.Now;
            _p = p;
            p.Start();

            // Start the IO threads (must be after p.Start())
            var log = new MemoryStream();
            var err = new MemoryStream();
            outputThread.Start(log);
            errorThread.Start(err);
            inputThread.Start(log);

            p.WaitForExit(); // flush the output buffer
            _end = DateTime.Now;

            //Console.Out.Write(output);
            //Console.Error.Write(error);
            var logPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            if (log.Length > 0)
            {
                // From https://stackoverflow.com/a/5504226
                // Seems to atomically open file, dump all and close it
                FileStream fs = null;
                try
                {
                    while (true)
                    {
                        try
                        {
                            fs = new FileStream(logPath + ".OUTPUT.log", FileMode.Append, FileSystemRights.AppendData,
                                                FileShare.Write, 4096, FileOptions.None);
                            break;
                        }
                        catch
                        {
                            Thread.Sleep(100);
                        }
                    }
                    var bytes = Encoding.UTF8.GetBytes(_start.ToString() + " " + allArguments + "  ------------------------------------------------------------------" + Environment.NewLine);
                    fs.Write(bytes, 0, bytes.Length);
                    log.Position = 0;
                    log.CopyTo(fs);
                    bytes = Encoding.UTF8.GetBytes(_end.ToString() + " " + allArguments + "  ------------------------------------------------------------------" + Environment.NewLine);
                    fs.Write(bytes, 0, bytes.Length);
                }
                finally
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
            if (err.Length > 0)
            {
                // From https://stackoverflow.com/a/5504226
                // Seems to atomically open file, dump all and close it
                FileStream fs = null;
                try
                {
                    while (true)
                    {
                        try
                        {
                            fs = new FileStream(logPath + ".ERROR.log", FileMode.Append, FileSystemRights.AppendData,
                                                FileShare.Write, 4096, FileOptions.None);
                            break;
                        }
                        catch
                        {
                            Thread.Sleep(100);
                        }
                    }
                    var bytes = Encoding.UTF8.GetBytes(_start.ToString() + " " + allArguments + "  ------------------------------------------------------------------" + Environment.NewLine);
                    fs.Write(bytes, 0, bytes.Length);
                    err.Position = 0;
                    err.CopyTo(fs);
                    bytes = Encoding.UTF8.GetBytes(_end.ToString() + " " + allArguments + "  ------------------------------------------------------------------" + Environment.NewLine);
                    fs.Write(bytes, 0, bytes.Length);
                }
                finally
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
            return p.ExitCode;
        }
        static Process _p;
        static DateTime _start;
        static DateTime _end;

        private static void outputReader(object tee)
        {
            // Pass the standard output of the child to our standard output
            passThrough(_p.StandardOutput.BaseStream, Console.OpenStandardOutput(), (Stream)tee);
        }

        private static void errorReader(object tee)
        {
            // Pass the standard error of the child to our standard error
            passThrough(_p.StandardError.BaseStream, Console.OpenStandardError(), (Stream)tee);
        }

        private static void inputReader(object tee)
        {
            // Pass our standard input into the standard input of the child
            passThrough(Console.OpenStandardInput(), _p.StandardInput.BaseStream, (Stream)tee);
        }

        /// <summary>
        /// Continuously copies data from one stream to the other.
        /// </summary>
        /// <param name="instream">The input stream.</param>
        /// <param name="outstream">The output stream.</param>
        private static void passThrough(Stream instream, Stream outstream, Stream tee)
        {
            byte[] buffer = new byte[4096];
            while (true)
            {
                int len;
                while ((len = instream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    lock(tee)
                    {
                        tee.Write(buffer, 0, len);
                        tee.Flush();
                    }
                    outstream.Write(buffer, 0, len);
                    outstream.Flush();
                }
            }
        }
    }
}
